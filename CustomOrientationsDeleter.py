bl_info = {
    "name": "Custom orientations deleter",
    "author": "Poll, Urko",
    "version": (0, 1, 0),
    "blender": (2, 83, 0),
    "location": "View 3D",
    "description": "Delete in 1 click all custom orientations",
    "warning": "development based on https://blenderartists.org/t/custom-transform-delete/582536/16",
    "wiki_url": "",
    "category": "3D View",
}

import bpy
from bpy.types import Operator

class OBJECT_OT_Delete_Custom_Orientations(bpy.types.Operator):
    
    """Delete custom orientations"""
    
    bl_idname = "object.del_cust_or"
    bl_label = "Delete custom orientations"
    bl_options = {'REGISTER', 'UNDO',}
    
    def execute(self, context):
        
        try:
            bpy.context.scene.transform_orientation_slots[0].type = ""
        except Exception as inst:
            transforms = []
            values_clean01 = str(inst).split("in")[1]
            values_clean02 = values_clean01.split("(")[1]
            values_clean03 = values_clean02.split(")")[0]
            values = values_clean03.split(", ")
            for val in values:
                transforms.append(val[1:-1])
            print(transforms)
        if transforms:
            transform_list = list(transforms)
            for type in transform_list[6:]:
                bpy.context.scene.transform_orientation_slots[0].type = type
                bpy.ops.transform.delete_orientation()
        return {'FINISHED'}
            
class INFO_MT_Delete_Custom_Orientations(bpy.types.Menu):
    
    bl_idname = "Delete custom orientations"
    bl_label = "Delete custom orientations"
    
    def draw(self, context):
        layout = self.layout
        layout.operator_context = 'INVOKE_REGION_WIN'
        row.operator('object.del_cust_or', text="Delete all custom orientations", icon="TRASH")

def DelCustOr_add_menu_func(self, context):
    self.layout.operator("object.del_cust_or", text="Delete all custom orientations", icon="TRASH")

addon_keymaps = []

def register():
    
    bpy.types.VIEW3D_PT_transform_orientations.append(DelCustOr_add_menu_func)
    bpy.utils.register_class(OBJECT_OT_Delete_Custom_Orientations)
    wm = bpy.context.window_manager
    km = wm.keyconfigs.addon.keymaps.new(name='3D View', space_type='VIEW_3D')
    kmi = km.keymap_items.new(
        OBJECT_OT_Delete_Custom_Orientations.bl_idname, 
        'Q', 'PRESS', shift=True, ctrl=False, alt=True)
    addon_keymaps.append((km, kmi))
    
def unregister():
    
    bpy.types.VIEW3D_PT_transform_orientations.remove(DelCustOr_add_menu_func)
    bpy.utils.unregister_class(OBJECT_OT_Delete_Custom_Orientations)
    for km, kmi in addon_keymaps:
            km.keymap_items.remove(kmi)
    addon_keymaps.clear()    


if __name__ == "__main__":
    register()
